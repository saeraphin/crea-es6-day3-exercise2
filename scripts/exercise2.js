import { cloneElementAndAppendIt, on } from './helpers.js'

(function exercise2() {
  /**
   * Écoutons attentivement.
   * 
   * 1. Rajouter un écouteur 'click' à l'élément '#step1-original'
   *  a. lors du clic, passer l'élément '#step1' à la méthode helper 'cloneElementAndAppendIt'
   *  b. votre élément devrait être cloné à chaque clic
   *  c. rajouter un écouteur délégué 'click' au <body> pour les éléments enfants '.clone' avec la méthode helper 'on'
   *  d. supprimer les clones lors du clic avec la méthode 'remove()'
   * 
   * 2. Rajouter un écouteur 'mousedown' à l'élément '#step2-btn'
   *  a. lors du 'mousedown' rajouter la classe 'mouse-down' à l'élément et retirer la class 'mouse-up'
   *  b. rajouter un écouteur 'mouseup' à l'élément '#step2-btn'
   *  c. lors du 'mouseup' rajouter la classe 'mouse-up' à l'élément et retirer la class 'mouse-down'
   * 
   * Bonus
   * 1. À la dernière étape du step1, au lieu de supprimer le clone, lui rajouter la classe 'to-be-deleted'
   *  a. la classe va déclencher une animation sur le clone
   *  b. récupérer les animations du clone et rajouter un écouteur 'finish' à la première de la liste
   *  c. lorsque l'animation est finie, retirer supprimer l'élément comme précédemment
   * 
   * 2. L'état du step2 n'est pas idéal, l'événement 'mouseup' se déclenche même si le 'mousedown' est survenu en dehors de l'élément
   *  a. ne rajouter l'écouteur 'mouseup' que lorsqu'il y a eu un 'mousedown' sur l'élément
   *  b. rajouter l'écouteur 'mouseup' pour qu'il soit unique (option 'once')
   * 
   * 3. L'état du bonus2 n'est toujours pas idéal, l'événement 'mouseup' ne se déclenche pas si le 'mouseup' est survenu en dehors de l'élément
   *  a. rajouter l'écouteur 'mouseup' sur le document, il se déclenchera alors du moment que l'utilisateur relâche la souris
   *  b. ce pattern nécessite (malheureusement) un pointeur vers le dernier élément à avoir reçu le focus
   *  c. stocker le dernier élément sur lequel est survenu un 'mousedown' dans une variable currentMouseDownTarget
   *  d. utiliser cette variable dans l'écouteur 'mouseup' du document, la vider après utilisation
   */



  // TODO #1
  const step1Original = document.getElementById('step1-original')



  // TODO #2
  const step2Btn = document.getElementById('step2-btn')

})()