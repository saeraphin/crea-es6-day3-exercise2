export const cloneElementAndAppendIt = element => {
  /** @type {HTMLElement} */
  const target = element
  const parent = target.parentNode
  /** @type {HTMLElement} */
  const clone = target.cloneNode(true)
  clone.classList.add('clone')
  parent.appendChild(clone)
}

// WARNING: this snippet is useful but imperfect
// https://flaviocopes.com/javascript-event-delegation/
export const on = (selector, eventType, childSelector, eventHandler) => {
  const elements = document.querySelectorAll(selector)
  for (let element of elements) {
    element.addEventListener(eventType, eventOnElement => {
      if (eventOnElement.target.matches(childSelector)) {
        eventHandler(eventOnElement)
      }
    })
  }
}